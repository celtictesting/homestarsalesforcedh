﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Clcik_Is_This_Current_Residence_Checkbox recording.
    /// </summary>
    [TestModule("8b7a62ce-2c6f-433b-971a-4a3a31d6ff61", ModuleType.Recording, 1)]
    public partial class Clcik_Is_This_Current_Residence_Checkbox : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Clcik_Is_This_Current_Residence_Checkbox instance = new Clcik_Is_This_Current_Residence_Checkbox();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Clcik_Is_This_Current_Residence_Checkbox()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Clcik_Is_This_Current_Residence_Checkbox Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking EnsureVisible() on item 'Salesforce_Website_Chrome.Asset_REO_Liability.SpanTagREO'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.SpanTagREOInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Asset_REO_Liability.SpanTagREO.EnsureVisible();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Asset_REO_Liability.Is_This_Current_Residence_Checkbox' at Center.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.Is_This_Current_Residence_CheckboxInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Asset_REO_Liability.Is_This_Current_Residence_Checkbox.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Checked to 'True' on item 'Salesforce_Website_Chrome.Asset_REO_Liability.Is_This_Current_Residence_Checkbox'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.Is_This_Current_Residence_CheckboxInfo, new RecordItemIndex(2));
            repo.Salesforce_Website_Chrome.Asset_REO_Liability.Is_This_Current_Residence_Checkbox.Element.SetAttributeValue("Checked", "True");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Checked='True') on item 'Salesforce_Website_Chrome.Asset_REO_Liability.Is_This_Current_Residence_Checkbox'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.Is_This_Current_Residence_CheckboxInfo, new RecordItemIndex(3));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Asset_REO_Liability.Is_This_Current_Residence_CheckboxInfo, "Checked", "True");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Visible='True') on item 'Salesforce_Website_Chrome.Asset_REO_Liability.SpanTagREO'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.SpanTagREOInfo, new RecordItemIndex(4));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Asset_REO_Liability.SpanTagREOInfo, "Visible", "True");
            Delay.Milliseconds(100);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
