﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Untick_Documents_Z_USDA recording.
    /// </summary>
    [TestModule("503ffde8-847f-4f34-b844-07756c2b88a6", ModuleType.Recording, 1)]
    public partial class Untick_Documents_Z_USDA : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Untick_Documents_Z_USDA instance = new Untick_Documents_Z_USDA();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Untick_Documents_Z_USDA()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Untick_Documents_Z_USDA Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Wait", "Waiting 5m to exist. Associated repository item: 'Encompass.Forms_Untick_Z.Unitck_Z_Form'", repo.Encompass.Forms_Untick_Z.Unitck_Z_FormInfo, new ActionTimeout(300000), new RecordItemIndex(0));
            repo.Encompass.Forms_Untick_Z.Unitck_Z_FormInfo.WaitForExists(300000);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Encompass.Forms_Untick_Z.Unitck_Z_Form' at .07;.09.", repo.Encompass.Forms_Untick_Z.Unitck_Z_FormInfo, new RecordItemIndex(1));
            repo.Encompass.Forms_Untick_Z.Unitck_Z_Form.Click(".07;.09");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Encompass.Forms_Untick_Z.Unitck_Z_Form' at .07;.15.", repo.Encompass.Forms_Untick_Z.Unitck_Z_FormInfo, new RecordItemIndex(2));
            repo.Encompass.Forms_Untick_Z.Unitck_Z_Form.Click(".07;.15");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Encompass.Forms_Untick_Z.Unitck_Z_Form' at .07;.6.", repo.Encompass.Forms_Untick_Z.Unitck_Z_FormInfo, new RecordItemIndex(3));
            repo.Encompass.Forms_Untick_Z.Unitck_Z_Form.Click(".07;.6");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Encompass.Forms_Untick_Z.Unitck_Z_Form' at .07;.67.", repo.Encompass.Forms_Untick_Z.Unitck_Z_FormInfo, new RecordItemIndex(4));
            repo.Encompass.Forms_Untick_Z.Unitck_Z_Form.Click(".07;.67");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Encompass.Forms_Untick_Z.Unitck_Z_Form' at .07;.83.", repo.Encompass.Forms_Untick_Z.Unitck_Z_FormInfo, new RecordItemIndex(5));
            repo.Encompass.Forms_Untick_Z.Unitck_Z_Form.Click(".07;.83");
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
