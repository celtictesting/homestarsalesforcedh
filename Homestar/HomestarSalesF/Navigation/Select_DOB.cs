﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Select_DOB recording.
    /// </summary>
    [TestModule("93831000-e833-407c-af6b-665aef7ae338", ModuleType.Recording, 1)]
    public partial class Select_DOB : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Select_DOB instance = new Select_DOB();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Select_DOB()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Select_DOB Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Convert_Lead_Dialog.Date_of_Birth_Calendar.Calendar_Icon' at Center.", repo.Salesforce_Website_Chrome.Convert_Lead_Dialog.Date_of_Birth_Calendar.Calendar_IconInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Convert_Lead_Dialog.Date_of_Birth_Calendar.Calendar_Icon.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Employment_Info.Pick_Year_In_Calendar' at Center.", repo.Salesforce_Website_Chrome.Employment_Info.Pick_Year_In_CalendarInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Employment_Info.Pick_Year_In_Calendar.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute TagValue to '1970' on item 'Salesforce_Website_Chrome.Employment_Info.Pick_Year_In_Calendar'.", repo.Salesforce_Website_Chrome.Employment_Info.Pick_Year_In_CalendarInfo, new RecordItemIndex(2));
            repo.Salesforce_Website_Chrome.Employment_Info.Pick_Year_In_Calendar.Element.SetAttributeValue("TagValue", "1970");
            Delay.Milliseconds(100);
            
            SelectDecember(repo.Salesforce_Website_Chrome.Employment_Info.GoToNextMonth1Info);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Borrower_CoBorrower.SpanTag9' at Center.", repo.Salesforce_Website_Chrome.Borrower_CoBorrower.SpanTag9Info, new RecordItemIndex(4));
            repo.Salesforce_Website_Chrome.Borrower_CoBorrower.SpanTag9.Click();
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
