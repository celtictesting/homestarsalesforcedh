﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Click_Save_And_Next_Loan_Info recording.
    /// </summary>
    [TestModule("09ad5f2f-435b-4d16-addb-4510ce2b1061", ModuleType.Recording, 1)]
    public partial class Click_Save_And_Next_Loan_Info : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Click_Save_And_Next_Loan_Info instance = new Click_Save_And_Next_Loan_Info();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Click_Save_And_Next_Loan_Info()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Click_Save_And_Next_Loan_Info Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            //Report.Log(ReportLevel.Info, "Mouse", "Mouse scroll Vertical by -600 units.", new RecordItemIndex(0));
            //Mouse.ScrollWheel(-600);
            //Delay.Milliseconds(500);
            
            //Report.Log(ReportLevel.Info, "Invoke action", "Invoking EnsureVisible() on item 'Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_Info'.", repo.Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_InfoInfo, new RecordItemIndex(1));
            //repo.Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_Info.EnsureVisible();
            //Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{End}'.", new RecordItemIndex(2));
            Keyboard.Press("{End}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 500ms.", new RecordItemIndex(3));
            Delay.Duration(500, false);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking EnsureVisible() on item 'Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_Info'.", repo.Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_InfoInfo, new RecordItemIndex(4));
            repo.Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_Info.EnsureVisible();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking PerformClick() on item 'Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_Info'.", repo.Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_InfoInfo, new RecordItemIndex(5));
            repo.Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_Info.PerformClick();
            Delay.Milliseconds(0);
            
            //Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_Info' at Center.", repo.Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_InfoInfo, new RecordItemIndex(6));
            //repo.Salesforce_Website_Chrome.Loan_Information.SaveAndNext_Loan_Info.Click();
            //Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
