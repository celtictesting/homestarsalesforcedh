﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Select_Date_From_B recording.
    /// </summary>
    [TestModule("c31bacf2-6138-4a64-ab57-f02b315d7822", ModuleType.Recording, 1)]
    public partial class Select_Date_From_B : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Select_Date_From_B instance = new Select_Date_From_B();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Select_Date_From_B()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Select_Date_From_B Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Employment_Info.Calentar_Icon_Date_From' at Center.", repo.Salesforce_Website_Chrome.Employment_Info.Calentar_Icon_Date_FromInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Employment_Info.Calentar_Icon_Date_From.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute TagValue to '2000' on item 'Salesforce_Website_Chrome.Employment_Info.Pick_Year_In_Calendar'.", repo.Salesforce_Website_Chrome.Employment_Info.Pick_Year_In_CalendarInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Employment_Info.Pick_Year_In_Calendar.Element.SetAttributeValue("TagValue", "2000");
            Delay.Milliseconds(100);
            
            SelectDecember(repo.Salesforce_Website_Chrome.Employment_Info_B_CoB.GoToNextMonth_In_Borrower_Previous_EmploymentInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Employment_Info_B_CoB.SpanTag26' at Center.", repo.Salesforce_Website_Chrome.Employment_Info_B_CoB.SpanTag26Info, new RecordItemIndex(3));
            repo.Salesforce_Website_Chrome.Employment_Info_B_CoB.SpanTag26.Click();
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
