﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Tick_Borrwer_Send_Consent recording.
    /// </summary>
    [TestModule("247f9805-2abd-4cf3-84ac-2df3329a1577", ModuleType.Recording, 1)]
    public partial class Tick_Borrwer_Send_Consent : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Tick_Borrwer_Send_Consent instance = new Tick_Borrwer_Send_Consent();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Tick_Borrwer_Send_Consent()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Tick_Borrwer_Send_Consent Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Wait", "Waiting 5m to exist. Associated repository item: 'Encompass.SendConsentRequestDialog.ChkBorrName'", repo.Encompass.SendConsentRequestDialog.ChkBorrNameInfo, new ActionTimeout(300000), new RecordItemIndex(0));
            repo.Encompass.SendConsentRequestDialog.ChkBorrNameInfo.WaitForExists(300000);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Encompass.SendConsentRequestDialog.ChkBorrName' at Center.", repo.Encompass.SendConsentRequestDialog.ChkBorrNameInfo, new RecordItemIndex(1));
            repo.Encompass.SendConsentRequestDialog.ChkBorrName.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Encompass.SendConsentRequestDialog.Send_Button_Send_Consent' at Center.", repo.Encompass.SendConsentRequestDialog.Send_Button_Send_ConsentInfo, new RecordItemIndex(2));
            repo.Encompass.SendConsentRequestDialog.Send_Button_Send_Consent.Click();
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
