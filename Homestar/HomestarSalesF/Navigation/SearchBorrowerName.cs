﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The SearchBorrowerName recording.
    /// </summary>
    [TestModule("500a3fdf-3675-4882-9159-beef41eaee7e", ModuleType.Recording, 1)]
    public partial class SearchBorrowerName : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static SearchBorrowerName instance = new SearchBorrowerName();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public SearchBorrowerName()
        {
            LastName = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static SearchBorrowerName Instance
        {
            get { return instance; }
        }

#region Variables

        string _LastName;

        /// <summary>
        /// Gets or sets the value of variable LastName.
        /// </summary>
        [TestVariable("e0da7a94-4939-4c2c-8113-fa7ccabf81f5")]
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Delay", "Waiting for 500ms.", new RecordItemIndex(0));
            Delay.Duration(500, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Right Click item 'Encompass.MainForm.Position' at 57;6.", repo.Encompass.MainForm.PositionInfo, new RecordItemIndex(1));
            repo.Encompass.MainForm.Position.Click(System.Windows.Forms.MouseButtons.Right, "57;6");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Encompass1.LeftEdge' at 40;9.", repo.Encompass1.LeftEdgeInfo, new RecordItemIndex(2));
            repo.Encompass1.LeftEdge.Click("40;9");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Firstimer Snr{Return}' with focus on 'Encompass.MainForm.BorrowerNameSearch'.", repo.Encompass.MainForm.BorrowerNameSearchInfo, new RecordItemIndex(3));
            repo.Encompass.MainForm.BorrowerNameSearch.PressKeys("Firstimer Snr{Return}");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
