﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Click_Next_Button recording.
    /// </summary>
    [TestModule("a28f69c8-d62c-41f1-ae52-431ae9f6208a", ModuleType.Recording, 1)]
    public partial class Click_Next_Button : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Click_Next_Button instance = new Click_Next_Button();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Click_Next_Button()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Click_Next_Button Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Dialog_Record_Type.Next_Button' at Center.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Dialog_Record_Type.Next_ButtonInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Dialog_Record_Type.Next_Button.Click();
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
