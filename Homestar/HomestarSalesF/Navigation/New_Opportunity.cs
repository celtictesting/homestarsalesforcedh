﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The New_Opportunity recording.
    /// </summary>
    [TestModule("afad9e68-3ab1-4101-a16d-bf696e39e10d", ModuleType.Recording, 1)]
    public partial class New_Opportunity : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesFRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesFRepository repo = global::HomestarSalesF.HomestarSalesFRepository.Instance;

        static New_Opportunity instance = new New_Opportunity();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public New_Opportunity()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static New_Opportunity Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'LightningExperienceSalesforce.New_Opportunity_Button' at Center.", repo.LightningExperienceSalesforce.New_Opportunity_ButtonInfo, new RecordItemIndex(0));
            repo.LightningExperienceSalesforce.New_Opportunity_Button.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='New Opportunity: Purchase or Refinance') on item 'LightningExperienceSalesforce.NewOpportunityPurchaseOrRefinance'.", repo.LightningExperienceSalesforce.NewOpportunityPurchaseOrRefinanceInfo, new RecordItemIndex(1));
            Validate.AttributeEqual(repo.LightningExperienceSalesforce.NewOpportunityPurchaseOrRefinanceInfo, "InnerText", "New Opportunity: Purchase or Refinance");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
