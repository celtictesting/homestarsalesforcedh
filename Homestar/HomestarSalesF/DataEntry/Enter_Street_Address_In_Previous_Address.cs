﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Street_Address_In_Previous_Address recording.
    /// </summary>
    [TestModule("4691d02a-5fa1-4488-a40e-cbb696f1b601", ModuleType.Recording, 1)]
    public partial class Enter_Street_Address_In_Previous_Address : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Enter_Street_Address_In_Previous_Address instance = new Enter_Street_Address_In_Previous_Address();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Street_Address_In_Previous_Address()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Street_Address_In_Previous_Address Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            //Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_Address' at Center.", repo.Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_AddressInfo, new RecordItemIndex(0));
            //repo.Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_Address.Click();
            //Delay.Milliseconds(200);
            
            //Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to '' on item 'Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_Address'.", repo.Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_AddressInfo, new RecordItemIndex(1));
            //repo.Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_Address.Element.SetAttributeValue("Value", "");
            //Delay.Milliseconds(0);
            
            //Report.Log(ReportLevel.Info, "Invoke action", "Invoking EnsureVisible() on item 'Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_Address'.", repo.Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_AddressInfo, new RecordItemIndex(2));
            //repo.Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_Address.EnsureVisible();
            //Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Next}'.", new RecordItemIndex(3));
            Keyboard.Press("{Next}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '123 TBD' with focus on 'Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_Address'.", repo.Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_AddressInfo, new RecordItemIndex(4));
            repo.Salesforce_Website_Chrome.Borrower_Information.Street_Address_In_Previous_Address.PressKeys("123 TBD");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Borrower_Information.Zip_In_Previous_Address' at Center.", repo.Salesforce_Website_Chrome.Borrower_Information.Zip_In_Previous_AddressInfo, new RecordItemIndex(5));
            repo.Salesforce_Website_Chrome.Borrower_Information.Zip_In_Previous_Address.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Tab}' with focus on 'Salesforce_Website_Chrome.Borrower_Information.Zip_In_Previous_Address'.", repo.Salesforce_Website_Chrome.Borrower_Information.Zip_In_Previous_AddressInfo, new RecordItemIndex(6));
            repo.Salesforce_Website_Chrome.Borrower_Information.Zip_In_Previous_Address.PressKeys("{Tab}");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
