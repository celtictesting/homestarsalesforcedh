﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Nearest_Living_Relative_Name recording.
    /// </summary>
    [TestModule("e6473a2b-20c9-44ae-b73d-540918db7abb", ModuleType.Recording, 1)]
    public partial class Enter_Nearest_Living_Relative_Name : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Enter_Nearest_Living_Relative_Name instance = new Enter_Nearest_Living_Relative_Name();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Nearest_Living_Relative_Name()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Nearest_Living_Relative_Name Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Wait", "Waiting 10s to exist. Associated repository item: 'DocuSign.Nearest_Relative_Name'", repo.DocuSign.Nearest_Relative_NameInfo, new ActionTimeout(10000), new RecordItemIndex(0));
            repo.DocuSign.Nearest_Relative_NameInfo.WaitForExists(10000);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'DocuSign.Nearest_Relative_Name' at Center.", repo.DocuSign.Nearest_Relative_NameInfo, new RecordItemIndex(1));
            repo.DocuSign.Nearest_Relative_Name.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Amy America' with focus on 'DocuSign.Nearest_Relative_Name'.", repo.DocuSign.Nearest_Relative_NameInfo, new RecordItemIndex(2));
            repo.DocuSign.Nearest_Relative_Name.PressKeys("Amy America");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'DocuSign.Relationship_To_You' at Center.", repo.DocuSign.Relationship_To_YouInfo, new RecordItemIndex(3));
            repo.DocuSign.Relationship_To_You.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Sister' with focus on 'DocuSign.Relationship_To_You'.", repo.DocuSign.Relationship_To_YouInfo, new RecordItemIndex(4));
            repo.DocuSign.Relationship_To_You.PressKeys("Sister");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
