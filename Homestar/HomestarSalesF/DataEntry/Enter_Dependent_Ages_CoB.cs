﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Dependent_Ages_CoB recording.
    /// </summary>
    [TestModule("98902122-d468-4390-98a3-a9b298368e79", ModuleType.Recording, 1)]
    public partial class Enter_Dependent_Ages_CoB : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Enter_Dependent_Ages_CoB instance = new Enter_Dependent_Ages_CoB();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Dependent_Ages_CoB()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Dependent_Ages_CoB Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Borrower_Information.Dependent_Ages_CoB' at Center.", repo.Salesforce_Website_Chrome.Borrower_Information.Dependent_Ages_CoBInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Borrower_Information.Dependent_Ages_CoB.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to '' on item 'Salesforce_Website_Chrome.Borrower_Information.Dependent_Ages_CoB'.", repo.Salesforce_Website_Chrome.Borrower_Information.Dependent_Ages_CoBInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Borrower_Information.Dependent_Ages_CoB.Element.SetAttributeValue("Value", "");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '11' with focus on 'Salesforce_Website_Chrome.Borrower_Information.Dependent_Ages_CoB'.", repo.Salesforce_Website_Chrome.Borrower_Information.Dependent_Ages_CoBInfo, new RecordItemIndex(2));
            repo.Salesforce_Website_Chrome.Borrower_Information.Dependent_Ages_CoB.PressKeys("11");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
