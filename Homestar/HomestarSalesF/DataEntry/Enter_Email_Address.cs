﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Email_Address recording.
    /// </summary>
    [TestModule("f2441d35-f0b8-4884-bcb8-4b58d99aee5a", ModuleType.Recording, 1)]
    public partial class Enter_Email_Address : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Enter_Email_Address instance = new Enter_Email_Address();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Email_Address()
        {
            Email = "alice@mailinator.com";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Email_Address Instance
        {
            get { return instance; }
        }

#region Variables

        string _Email;

        /// <summary>
        /// Gets or sets the value of variable Email.
        /// </summary>
        [TestVariable("29b381f2-fb2b-4520-a6f7-1466ade926e0")]
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'Salesforce_Website_Chrome'.", repo.Salesforce_Website_Chrome.SelfInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Self.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_FieldInfo, new RecordItemIndex(1));
            Validate.Exists(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_FieldInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_Field' at Center.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_FieldInfo, new RecordItemIndex(2));
            repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_Field.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to '' on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_FieldInfo, new RecordItemIndex(3));
            repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_Field.Element.SetAttributeValue("Value", "");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(4));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$Email' with focus on 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_FieldInfo, new RecordItemIndex(5));
            repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_Field.PressKeys(Email);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(6));
            Delay.Duration(1000, false);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
