﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Year_Built recording.
    /// </summary>
    [TestModule("687d0f37-65b3-4ba0-bb17-eaa0416a039f", ModuleType.Recording, 1)]
    public partial class Enter_Year_Built : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Enter_Year_Built instance = new Enter_Year_Built();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Year_Built()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Year_Built Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'Salesforce_Website_Chrome'.", repo.Salesforce_Website_Chrome.SelfInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Self.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_Info' at Center.", repo.Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_InfoInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_Info.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to '' on item 'Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_Info'.", repo.Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_InfoInfo, new RecordItemIndex(2));
            repo.Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_Info.Element.SetAttributeValue("Value", "");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(3));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '2000' with focus on 'Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_Info'.", repo.Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_InfoInfo, new RecordItemIndex(4));
            repo.Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_Info.PressKeys("2000");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value='2000') on item 'Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_Info'.", repo.Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_InfoInfo, new RecordItemIndex(5));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Property_Information.YearBuilt_In_Property_InfoInfo, "Value", "2000");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
