﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Last_Name recording.
    /// </summary>
    [TestModule("4ada6533-d81a-4a48-99b5-9d0881e3490a", ModuleType.Recording, 1)]
    public partial class Enter_Last_Name : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Enter_Last_Name instance = new Enter_Last_Name();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Last_Name()
        {
            LastName = "America";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Last_Name Instance
        {
            get { return instance; }
        }

#region Variables

        string _LastName;

        /// <summary>
        /// Gets or sets the value of variable LastName.
        /// </summary>
        [TestVariable("87b2061d-6b5e-4f6d-951a-497d385a523f")]
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_Field' at Center.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_FieldInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_Field.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'America' with focus on 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_FieldInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_Field.PressKeys("America");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Tab}' with focus on 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_FieldInfo, new RecordItemIndex(2));
            repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_Field.PressKeys("{Tab}");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
