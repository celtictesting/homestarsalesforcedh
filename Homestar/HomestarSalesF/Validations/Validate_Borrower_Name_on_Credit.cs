﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Borrower_Name_on_Credit recording.
    /// </summary>
    [TestModule("39c1ad91-215c-4019-bba2-497d43e3ae08", ModuleType.Recording, 1)]
    public partial class Validate_Borrower_Name_on_Credit : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Borrower_Name_on_Credit instance = new Validate_Borrower_Name_on_Credit();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Borrower_Name_on_Credit()
        {
            borrowerNameText = "";
            firstName = "";
            lastName = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Borrower_Name_on_Credit Instance
        {
            get { return instance; }
        }

#region Variables

        string _borrowerNameText;

        /// <summary>
        /// Gets or sets the value of variable borrowerNameText.
        /// </summary>
        [TestVariable("f8151950-91f5-4208-8a1f-c1d12228f127")]
        public string borrowerNameText
        {
            get { return _borrowerNameText; }
            set { _borrowerNameText = value; }
        }

        string _firstName;

        /// <summary>
        /// Gets or sets the value of variable firstName.
        /// </summary>
        [TestVariable("99121a75-c23d-4098-aa95-e209439bae51")]
        public string firstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        string _lastName;

        /// <summary>
        /// Gets or sets the value of variable lastName.
        /// </summary>
        [TestVariable("87c1438e-c7e0-492f-8d59-93865ace35af")]
        public string lastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'TagValue' from item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.BorrowerFirstName_In_Credit_Information' and assigning its value to variable 'firstName'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.BorrowerFirstName_In_Credit_InformationInfo, new RecordItemIndex(0));
            firstName = repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.BorrowerFirstName_In_Credit_Information.Element.GetAttributeValueText("TagValue");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'TagValue' from item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.BorrowerLastName_In_Credit_Information' and assigning its value to variable 'lastName'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.BorrowerLastName_In_Credit_InformationInfo, new RecordItemIndex(1));
            lastName = repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.BorrowerLastName_In_Credit_Information.Element.GetAttributeValueText("TagValue");
            Delay.Milliseconds(0);
            
            CreateBorrowerNameVariable();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeContains (InnerText>$borrowerNameText) on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Borrower_Name_In_Credit_Info'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Borrower_Name_In_Credit_InfoInfo, new RecordItemIndex(3));
            Validate.AttributeContains(repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Borrower_Name_In_Credit_InfoInfo, "InnerText", borrowerNameText);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
