﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Home_Num_Borrower recording.
    /// </summary>
    [TestModule("83c54cc6-928c-4b4e-95b8-e0b04a9c9c8a", ModuleType.Recording, 1)]
    public partial class Validate_Home_Num_Borrower : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Home_Num_Borrower instance = new Validate_Home_Num_Borrower();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Home_Num_Borrower()
        {
            HomePhoneNum = "456-789-1234";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Home_Num_Borrower Instance
        {
            get { return instance; }
        }

#region Variables

        string _HomePhoneNum;

        /// <summary>
        /// Gets or sets the value of variable HomePhoneNum.
        /// </summary>
        [TestVariable("287eba23-1c46-4fc6-b098-0ce808ed7629")]
        public string HomePhoneNum
        {
            get { return _HomePhoneNum; }
            set { _HomePhoneNum = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking EnsureVisible() on item 'Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_Info'.", repo.Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_InfoInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_Info.EnsureVisible();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_Info' at Center.", repo.Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_InfoInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_Info.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Tab}'.", new RecordItemIndex(2));
            Keyboard.Press("{Tab}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeNotEqual (Value!='') on item 'Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_Info'.", repo.Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_InfoInfo, new RecordItemIndex(3));
            Validate.AttributeNotEqual(repo.Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_InfoInfo, "Value", "");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value=$HomePhoneNum) on item 'Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_Info'.", repo.Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_InfoInfo, new RecordItemIndex(4));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Borrower_Information.Home_Phone_Nr_In_Borrower_InfoInfo, "Value", HomePhoneNum);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
