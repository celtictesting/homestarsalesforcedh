﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Logged_In recording.
    /// </summary>
    [TestModule("c23271c9-3295-4af9-8270-25dd407e7a79", ModuleType.Recording, 1)]
    public partial class Validate_Logged_In : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Logged_In instance = new Validate_Logged_In();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Logged_In()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Logged_In Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Wait", "Waiting 1m to exist. Associated repository item: 'Encompass.EncompassHomePage.Welcome'", repo.Encompass.EncompassHomePage.WelcomeInfo, new ActionTimeout(60000), new RecordItemIndex(0));
            repo.Encompass.EncompassHomePage.WelcomeInfo.WaitForExists(60000);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'Encompass.EncompassHomePage.Welcome'.", repo.Encompass.EncompassHomePage.WelcomeInfo, new RecordItemIndex(1));
            Validate.Exists(repo.Encompass.EncompassHomePage.WelcomeInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='Deborah') on item 'Encompass.EncompassHomePage.Username'.", repo.Encompass.EncompassHomePage.UsernameInfo, new RecordItemIndex(2));
            Validate.AttributeEqual(repo.Encompass.EncompassHomePage.UsernameInfo, "InnerText", "Deborah");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
