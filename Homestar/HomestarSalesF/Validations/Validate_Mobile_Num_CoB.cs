﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Mobile_Num_CoB recording.
    /// </summary>
    [TestModule("a8d1009e-5f91-4c2b-b5bc-7563e169f0fe", ModuleType.Recording, 1)]
    public partial class Validate_Mobile_Num_CoB : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Mobile_Num_CoB instance = new Validate_Mobile_Num_CoB();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Mobile_Num_CoB()
        {
            MobileNum = "321-654-9875";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Mobile_Num_CoB Instance
        {
            get { return instance; }
        }

#region Variables

        string _MobileNum;

        /// <summary>
        /// Gets or sets the value of variable MobileNum.
        /// </summary>
        [TestVariable("061cdee7-21c2-4a2d-ba8c-4124a77e89a7")]
        public string MobileNum
        {
            get { return _MobileNum; }
            set { _MobileNum = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking EnsureVisible() on item 'Salesforce_Website_Chrome.Borrower_Information.Co_BorrowerInformation_Heading'.", repo.Salesforce_Website_Chrome.Borrower_Information.Co_BorrowerInformation_HeadingInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Borrower_Information.Co_BorrowerInformation_Heading.EnsureVisible();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking PerformClick() on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_Info'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_InfoInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_Info.PerformClick();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to '' on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_Info'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_InfoInfo, new RecordItemIndex(2));
            repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_Info.Element.SetAttributeValue("Value", "");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$MobileNum' with focus on 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_Info'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_InfoInfo, new RecordItemIndex(3));
            repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_Info.PressKeys(MobileNum);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value=$MobileNum) on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_Info'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_InfoInfo, new RecordItemIndex(4));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Mobile_Num_In_Co_Borrower_InfoInfo, "Value", MobileNum);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
