﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Credit_Reference_Number_Populated recording.
    /// </summary>
    [TestModule("17aaa311-63b3-4b0d-9d83-7f9b60f03531", ModuleType.Recording, 1)]
    public partial class Validate_Credit_Reference_Number_Populated : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Credit_Reference_Number_Populated instance = new Validate_Credit_Reference_Number_Populated();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Credit_Reference_Number_Populated()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Credit_Reference_Number_Populated Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Delay", "Waiting for 5s.", new RecordItemIndex(0));
            Delay.Duration(5000, false);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Visible='True') on item 'Salesforce_Website_Chrome.Credit_Information.Credit_Reference_Number_Field'.", repo.Salesforce_Website_Chrome.Credit_Information.Credit_Reference_Number_FieldInfo, new RecordItemIndex(1));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Credit_Information.Credit_Reference_Number_FieldInfo, "Visible", "True");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeNotEqual (Value!='') on item 'Salesforce_Website_Chrome.Credit_Information.Credit_Reference_Number_Field'.", repo.Salesforce_Website_Chrome.Credit_Information.Credit_Reference_Number_FieldInfo, new RecordItemIndex(2));
            Validate.AttributeNotEqual(repo.Salesforce_Website_Chrome.Credit_Information.Credit_Reference_Number_FieldInfo, "Value", "");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
