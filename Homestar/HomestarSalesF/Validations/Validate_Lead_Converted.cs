﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Lead_Converted recording.
    /// </summary>
    [TestModule("c4dfc0cf-bf34-4986-8faa-7b4498fe4098", ModuleType.Recording, 1)]
    public partial class Validate_Lead_Converted : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Lead_Converted instance = new Validate_Lead_Converted();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Lead_Converted()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Lead_Converted Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Wait", "Waiting 50s to exist. Associated repository item: 'Salesforce_Website_Chrome.Convert_Lead_Dialog.Lead_Converted_Message_Text'", repo.Salesforce_Website_Chrome.Convert_Lead_Dialog.Lead_Converted_Message_TextInfo, new ActionTimeout(50000), new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Convert_Lead_Dialog.Lead_Converted_Message_TextInfo.WaitForExists(50000);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='Lead converted successfully.') on item 'Salesforce_Website_Chrome.Convert_Lead_Dialog.Lead_Converted_Message_Text'.", repo.Salesforce_Website_Chrome.Convert_Lead_Dialog.Lead_Converted_Message_TextInfo, new RecordItemIndex(1));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Convert_Lead_Dialog.Lead_Converted_Message_TextInfo, "InnerText", "Lead converted successfully.");
            Delay.Milliseconds(100);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
