﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Lead_Information recording.
    /// </summary>
    [TestModule("02466a0b-1ac0-4761-9387-2c9dcab8c343", ModuleType.Recording, 1)]
    public partial class Validate_Lead_Information : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Lead_Information instance = new Validate_Lead_Information();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Lead_Information()
        {
            firstName = "Andy";
            lastName = "America";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Lead_Information Instance
        {
            get { return instance; }
        }

#region Variables

        string _firstName;

        /// <summary>
        /// Gets or sets the value of variable firstName.
        /// </summary>
        [TestVariable("6e0ba307-6db0-42ea-b354-8161a748cbd8")]
        public string firstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        string _lastName;

        /// <summary>
        /// Gets or sets the value of variable lastName.
        /// </summary>
        [TestVariable("4fc10309-b196-4d6f-85a3-b5b2d28fb70f")]
        public string lastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='Made Contact') on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Lead_Status_DropDown'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Lead_Status_DropDownInfo, new RecordItemIndex(0));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Lead_Status_DropDownInfo, "InnerText", "Made Contact");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='Dr.') on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Salutation_DropDown'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Salutation_DropDownInfo, new RecordItemIndex(1));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Salutation_DropDownInfo, "InnerText", "Dr.");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value=$firstName) on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.First_Name_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.First_Name_FieldInfo, new RecordItemIndex(2));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.First_Name_FieldInfo, "Value", firstName);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value='Test') on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Middle_Name_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Middle_Name_FieldInfo, new RecordItemIndex(3));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Middle_Name_FieldInfo, "Value", "Test");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value=$lastName) on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_FieldInfo, new RecordItemIndex(4));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Last_Name_FieldInfo, "Value", lastName);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value='Snr') on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Suffix_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Suffix_FieldInfo, new RecordItemIndex(5));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Lead_Information_Contact_Details.Suffix_FieldInfo, "Value", "Snr");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
