﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Oppertunity_Info recording.
    /// </summary>
    [TestModule("f0b8de01-d1ef-40a2-8369-31a8839c950b", ModuleType.Recording, 1)]
    public partial class Validate_Oppertunity_Info : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Oppertunity_Info instance = new Validate_Oppertunity_Info();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Oppertunity_Info()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Oppertunity_Info Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Delay", "Waiting for 6s.", new RecordItemIndex(0));
            Delay.Duration(6000, false);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{F5}' with focus on 'Salesforce_Website_Chrome'.", repo.Salesforce_Website_Chrome.SelfInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Self.EnsureVisible();
            Keyboard.Press("{F5}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 500ms.", new RecordItemIndex(2));
            Delay.Duration(500, false);
            
            Report.Log(ReportLevel.Info, "Wait", "Waiting 30s to exist. Associated repository item: 'Salesforce_Website_Chrome.Oppertunity_Info_Borrower_CoBorrower.Edit_Info_Pencil_Icon'", repo.Salesforce_Website_Chrome.Oppertunity_Info_Borrower_CoBorrower.Edit_Info_Pencil_IconInfo, new ActionTimeout(30000), new RecordItemIndex(3));
            repo.Salesforce_Website_Chrome.Oppertunity_Info_Borrower_CoBorrower.Edit_Info_Pencil_IconInfo.WaitForExists(30000);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Oppertunity_Info_Borrower_CoBorrower.Edit_Info_Pencil_Icon' at Center.", repo.Salesforce_Website_Chrome.Oppertunity_Info_Borrower_CoBorrower.Edit_Info_Pencil_IconInfo, new RecordItemIndex(4));
            repo.Salesforce_Website_Chrome.Oppertunity_Info_Borrower_CoBorrower.Edit_Info_Pencil_Icon.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='Andy Test America Snr') on item 'Salesforce_Website_Chrome.Opportunities_Page.Text.Primary_Borrower_Name'.", repo.Salesforce_Website_Chrome.Opportunities_Page.Text.Primary_Borrower_NameInfo, new RecordItemIndex(5));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Opportunities_Page.Text.Primary_Borrower_NameInfo, "TagValue", "Andy Test America Snr");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue='Amy America') on item 'Salesforce_Website_Chrome.Oppertunity_Info_Borrower_CoBorrower.CoBorrower_Name'.", repo.Salesforce_Website_Chrome.Oppertunity_Info_Borrower_CoBorrower.CoBorrower_NameInfo, new RecordItemIndex(6));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Oppertunity_Info_Borrower_CoBorrower.CoBorrower_NameInfo, "TagValue", "Amy America");
            Delay.Milliseconds(100);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
