﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// Your custom recording code should go in this file.
// The designer will only add methods to this file, so your custom code won't be overwritten.
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace HomestarSalesF.Validations
{
    public partial class Validate_Credit_Expiration_Date
    {
        /// <summary>
        /// This method gets called right after the recording has been started.
        /// It can be used to execute recording specific initialization code.
        /// </summary>
        private void Init()
        {
            // Your recording specific initialization code goes here.
        }

        public void validateExpirationDate()
        {
        	// Convert actual date from screen, from a string, to a date time variable
            var actualDate = System.DateTime.Parse(actualExpirationDate);
			Report.Info("actualDate from screen: " + actualDate.ToShortDateString());

			// Assign Today's date to a variable
        	var todaysDate = System.DateTime.Today;
            
            // Calculate the date 120 days from today and assign it to a variable - this is the expected expiration date
            var expectedExpirationDate = todaysDate.AddDays(120);
            Report.Info("expectedExpirationDate: " + expectedExpirationDate.ToShortDateString());           

			// Validate that actual date is same as expected date - pass test if it is, fail test if it is not
			if (actualDate == expectedExpirationDate)
				Report.Success("Credit Expiration Date on screen is 120 days from today's date.");
			else
				Report.Failure("Credit Expiration Date on screen is incorrect - it is not 120 days from today's date!");            
            
        }

    }
}
