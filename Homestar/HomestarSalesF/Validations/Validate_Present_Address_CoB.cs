﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Present_Address_CoB recording.
    /// </summary>
    [TestModule("0560f01f-453c-467c-8bd0-626ead6e1dac", ModuleType.Recording, 1)]
    public partial class Validate_Present_Address_CoB : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Present_Address_CoB instance = new Validate_Present_Address_CoB();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Present_Address_CoB()
        {
            Zip = "02723";
            State = "MA";
            Street = "4321 Cul de Sac Street";
            City = "Someplace";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Present_Address_CoB Instance
        {
            get { return instance; }
        }

#region Variables

        string _Zip;

        /// <summary>
        /// Gets or sets the value of variable Zip.
        /// </summary>
        [TestVariable("b4e286ef-1dc4-4f87-87ef-5b5ff0f21453")]
        public string Zip
        {
            get { return _Zip; }
            set { _Zip = value; }
        }

        string _State;

        /// <summary>
        /// Gets or sets the value of variable State.
        /// </summary>
        [TestVariable("bc0d2f05-f0f8-477b-86d4-e909c7a89985")]
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        string _Street;

        /// <summary>
        /// Gets or sets the value of variable Street.
        /// </summary>
        [TestVariable("619825b9-5cc3-43e9-ac5d-c6bbfaa17922")]
        public string Street
        {
            get { return _Street; }
            set { _Street = value; }
        }

        string _City;

        /// <summary>
        /// Gets or sets the value of variable City.
        /// </summary>
        [TestVariable("09f0b873-5483-4b10-9758-38e43863589b")]
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking EnsureVisible() on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.PresentAddress'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.PresentAddressInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.PresentAddress.EnsureVisible();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking EnsureVisible() on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Same_Address_Checkbox_CoBorrower'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Same_Address_Checkbox_CoBorrowerInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Same_Address_Checkbox_CoBorrower.EnsureVisible();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking PerformClick() on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Same_Address_Checkbox_CoBorrower'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Same_Address_Checkbox_CoBorrowerInfo, new RecordItemIndex(2));
            repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Same_Address_Checkbox_CoBorrower.PerformClick();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Checked='True') on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Same_Address_Checkbox_CoBorrower'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Same_Address_Checkbox_CoBorrowerInfo, new RecordItemIndex(3));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Same_Address_Checkbox_CoBorrowerInfo, "Checked", "True");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value=$Zip) on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Zip_In_CoBorrower_Present_Address'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Zip_In_CoBorrower_Present_AddressInfo, new RecordItemIndex(4));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Zip_In_CoBorrower_Present_AddressInfo, "Value", Zip);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value=$State) on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.State_In_CoBorrower_Present_Address'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.State_In_CoBorrower_Present_AddressInfo, new RecordItemIndex(5));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.State_In_CoBorrower_Present_AddressInfo, "Value", State);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value=$Street) on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Street_In_CoBorrower_Present_Address'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Street_In_CoBorrower_Present_AddressInfo, new RecordItemIndex(6));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.Street_In_CoBorrower_Present_AddressInfo, "Value", Street);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value=$City) on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.City_In_CoBorrower_Present_Address'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.City_In_CoBorrower_Present_AddressInfo, new RecordItemIndex(7));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.City_In_CoBorrower_Present_AddressInfo, "Value", City);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
