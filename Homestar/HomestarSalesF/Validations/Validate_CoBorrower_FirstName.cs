﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_CoBorrower_FirstName recording.
    /// </summary>
    [TestModule("4b1cb400-add3-4154-a1b2-0965f138c67f", ModuleType.Recording, 1)]
    public partial class Validate_CoBorrower_FirstName : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_CoBorrower_FirstName instance = new Validate_CoBorrower_FirstName();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_CoBorrower_FirstName()
        {
            FirstName = "Amy";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_CoBorrower_FirstName Instance
        {
            get { return instance; }
        }

#region Variables

        string _FirstName;

        /// <summary>
        /// Gets or sets the value of variable FirstName.
        /// </summary>
        [TestVariable("3c70e35a-77ea-4fd9-b8ec-0fc3b163e81c")]
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking EnsureVisible() on item 'Salesforce_Website_Chrome.Borrower_Information.Co_BorrowerInformation_Heading'.", repo.Salesforce_Website_Chrome.Borrower_Information.Co_BorrowerInformation_HeadingInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Borrower_Information.Co_BorrowerInformation_Heading.EnsureVisible();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeNotEqual (Value!='') on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoBorrower_First_Name'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoBorrower_First_NameInfo, new RecordItemIndex(1));
            Validate.AttributeNotEqual(repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoBorrower_First_NameInfo, "Value", "");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value='Amy') on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoBorrower_First_Name'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoBorrower_First_NameInfo, new RecordItemIndex(2));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoBorrower_First_NameInfo, "Value", "Amy");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
