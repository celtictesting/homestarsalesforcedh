﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Net_Worth_ recording.
    /// </summary>
    [TestModule("1ac6d274-94d3-482a-968c-75c142be2550", ModuleType.Recording, 1)]
    public partial class Validate_Net_Worth_ : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Net_Worth_ instance = new Validate_Net_Worth_();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Net_Worth_()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Net_Worth_ Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='Total Assets Value: $285,000.00') on item 'Salesforce_Website_Chrome.Asset_REO_Liability.TotalAssetsValue_In_Liability_Banner'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.TotalAssetsValue_In_Liability_BannerInfo, new RecordItemIndex(0));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Asset_REO_Liability.TotalAssetsValue_In_Liability_BannerInfo, "InnerText", "Total Assets Value: $285,000.00");
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeContains (InnerText>'Total Liabilities: $') on item 'Salesforce_Website_Chrome.Asset_REO_Liability.TotalLiabilities_In_Liabilites_Banner'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.TotalLiabilities_In_Liabilites_BannerInfo, new RecordItemIndex(1));
            Validate.AttributeContains(repo.Salesforce_Website_Chrome.Asset_REO_Liability.TotalLiabilities_In_Liabilites_BannerInfo, "InnerText", "Total Liabilities: $");
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeContains (InnerText>'Net Worth: $') on item 'Salesforce_Website_Chrome.Asset_REO_Liability.Net_Worth_In_Liabilities_Banner'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.Net_Worth_In_Liabilities_BannerInfo, new RecordItemIndex(2));
            Validate.AttributeContains(repo.Salesforce_Website_Chrome.Asset_REO_Liability.Net_Worth_In_Liabilities_BannerInfo, "InnerText", "Net Worth: $");
            Delay.Milliseconds(100);
            
            // Subtract total liabilities - total assets value = net worth
            ValidateNetWorth();
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
