﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Vaildate_MIP_Are_Prepopulated recording.
    /// </summary>
    [TestModule("81e64e6c-6708-4850-8898-775db495d64f", ModuleType.Recording, 1)]
    public partial class Vaildate_MIP_Are_Prepopulated : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Vaildate_MIP_Are_Prepopulated instance = new Vaildate_MIP_Are_Prepopulated();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Vaildate_MIP_Are_Prepopulated()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Vaildate_MIP_Are_Prepopulated Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Visible='True') on item 'Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FF_Percent__In_MIP_PMI_Guarantee_Calculation'.", repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FF_Percent__In_MIP_PMI_Guarantee_CalculationInfo, new RecordItemIndex(0));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FF_Percent__In_MIP_PMI_Guarantee_CalculationInfo, "Visible", "True");
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeContains (Value>'%') on item 'Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FF_Percent__In_MIP_PMI_Guarantee_Calculation'.", repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FF_Percent__In_MIP_PMI_Guarantee_CalculationInfo, new RecordItemIndex(1));
            Validate.AttributeContains(repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FF_Percent__In_MIP_PMI_Guarantee_CalculationInfo, "Value", "%");
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeNotEqual (Value!='') on item 'Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FF_Percent__In_MIP_PMI_Guarantee_Calculation'.", repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FF_Percent__In_MIP_PMI_Guarantee_CalculationInfo, new RecordItemIndex(2));
            Validate.AttributeNotEqual(repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FF_Percent__In_MIP_PMI_Guarantee_CalculationInfo, "Value", "");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Visible='True') on item 'Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_Field'.", repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FieldInfo, new RecordItemIndex(3));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FieldInfo, "Visible", "True");
            Delay.Milliseconds(100);
            
            //Report.Log(ReportLevel.Info, "Validation", "Validating AttributeContains (Value>'$') on item 'Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_Field'.", repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FieldInfo, new RecordItemIndex(4));
            //Validate.AttributeContains(repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FieldInfo, "Value", "$");
            //Delay.Milliseconds(100);
            
            //Report.Log(ReportLevel.Info, "Validation", "Validating AttributeNotEqual (Value!='') on item 'Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_Field'.", repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FieldInfo, new RecordItemIndex(5));
            //Validate.AttributeNotEqual(repo.Salesforce_Website_Chrome.Loan_Information.Veteran_Loan.MIP_FieldInfo, "Value", "");
            //Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
