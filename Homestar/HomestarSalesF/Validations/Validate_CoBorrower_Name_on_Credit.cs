﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_CoBorrower_Name_on_Credit recording.
    /// </summary>
    [TestModule("ffa578db-c2b3-46af-9908-265cf68463d1", ModuleType.Recording, 1)]
    public partial class Validate_CoBorrower_Name_on_Credit : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_CoBorrower_Name_on_Credit instance = new Validate_CoBorrower_Name_on_Credit();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_CoBorrower_Name_on_Credit()
        {
            CoFirstName = "";
            CoLastName = "";
            CoBorrowerNameText = "Co borrower: Amy America";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_CoBorrower_Name_on_Credit Instance
        {
            get { return instance; }
        }

#region Variables

        string _CoFirstName;

        /// <summary>
        /// Gets or sets the value of variable CoFirstName.
        /// </summary>
        [TestVariable("5e16ee5d-b0bd-4f41-9ca9-812009c6a511")]
        public string CoFirstName
        {
            get { return _CoFirstName; }
            set { _CoFirstName = value; }
        }

        string _CoLastName;

        /// <summary>
        /// Gets or sets the value of variable CoLastName.
        /// </summary>
        [TestVariable("123d60a8-2cf1-42cd-9837-b6dd0c9a9d78")]
        public string CoLastName
        {
            get { return _CoLastName; }
            set { _CoLastName = value; }
        }

        string _CoBorrowerNameText;

        /// <summary>
        /// Gets or sets the value of variable CoBorrowerNameText.
        /// </summary>
        [TestVariable("adbecdc5-f433-42f9-bfc4-5203bde0975e")]
        public string CoBorrowerNameText
        {
            get { return _CoBorrowerNameText; }
            set { _CoBorrowerNameText = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'Value' from item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoborrowerFirstName_In_Credit_Information' and assigning its value to variable 'CoFirstName'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoborrowerFirstName_In_Credit_InformationInfo, new RecordItemIndex(0));
            CoFirstName = repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoborrowerFirstName_In_Credit_Information.Element.GetAttributeValueText("Value");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'Value' from item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoborrowerLastName_In_Credit_Information' and assigning its value to variable 'CoLastName'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoborrowerLastName_In_Credit_InformationInfo, new RecordItemIndex(1));
            CoLastName = repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoborrowerLastName_In_Credit_Information.Element.GetAttributeValueText("Value");
            Delay.Milliseconds(0);
            
            CreateCoBorrowerNameVariable();
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText=$CoBorrowerNameText) on item 'Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoBorrowerAmyAmerica_In_Credit'.", repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoBorrowerAmyAmerica_In_CreditInfo, new RecordItemIndex(3));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Credit_Info_Borrower_CoBorrwer.CoBorrowerAmyAmerica_In_CreditInfo, "InnerText", CoBorrowerNameText);
            Delay.Milliseconds(100);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
