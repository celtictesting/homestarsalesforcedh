﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Contact_Information recording.
    /// </summary>
    [TestModule("b35e7257-f87e-4f91-9598-3d07a137d046", ModuleType.Recording, 1)]
    public partial class Validate_Contact_Information : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Contact_Information instance = new Validate_Contact_Information();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Contact_Information()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Contact_Information Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value='6781236549') on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Cell_Phone_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Cell_Phone_FieldInfo, new RecordItemIndex(0));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Cell_Phone_FieldInfo, "Value", "6781236549");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value='8663435628') on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Home_Phone_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Home_Phone_FieldInfo, new RecordItemIndex(1));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Home_Phone_FieldInfo, "Value", "8663435628");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value='8663435734') on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Office_Phone_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Office_Phone_FieldInfo, new RecordItemIndex(2));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Office_Phone_FieldInfo, "Value", "8663435734");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeContains (Value>'afirstimer@mailinator.com') on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_Field'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_FieldInfo, new RecordItemIndex(3));
            Validate.AttributeContains(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Email_FieldInfo, "Value", "afirstimer@mailinator.com");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking EnsureVisible() on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Preferred_Contact_Method_DropDown'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Preferred_Contact_Method_DropDownInfo, new RecordItemIndex(4));
            repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Preferred_Contact_Method_DropDown.EnsureVisible();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='Email') on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Preferred_Contact_Method_DropDown'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Preferred_Contact_Method_DropDownInfo, new RecordItemIndex(5));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Preferred_Contact_Method_DropDownInfo, "InnerText", "Email");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='Afternoon') on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Best_Contact_Time_DropDown'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Best_Contact_Time_DropDownInfo, new RecordItemIndex(6));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Best_Contact_Time_DropDownInfo, "InnerText", "Afternoon");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='LinkedIn') on item 'Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Preferred_Social_Media_Format_DropDown'.", repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Preferred_Social_Media_Format_DropDownInfo, new RecordItemIndex(7));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Leads_Page.New_Lead_Borrower_CoBorrower_Dialog.Contact_Information.Preferred_Social_Media_Format_DropDownInfo, "InnerText", "LinkedIn");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
