﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarSalesF.Validations
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Mobile_Phone_Num_Is_Prepopulated recording.
    /// </summary>
    [TestModule("c4e5b23a-c26d-4ca6-ab84-6d0dbb1ae002", ModuleType.Recording, 1)]
    public partial class Validate_Mobile_Phone_Num_Is_Prepopulated : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarSalesF.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarSalesF.HomestarSalesforceRepository repo = global::HomestarSalesF.HomestarSalesforceRepository.Instance;

        static Validate_Mobile_Phone_Num_Is_Prepopulated instance = new Validate_Mobile_Phone_Num_Is_Prepopulated();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Mobile_Phone_Num_Is_Prepopulated()
        {
            MobileNum = "6781236549";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Mobile_Phone_Num_Is_Prepopulated Instance
        {
            get { return instance; }
        }

#region Variables

        string _MobileNum;

        /// <summary>
        /// Gets or sets the value of variable MobileNum.
        /// </summary>
        [TestVariable("66e7d91e-ade3-46ed-9109-6719fe25e972")]
        public string MobileNum
        {
            get { return _MobileNum; }
            set { _MobileNum = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Borrower_Information.Mobile_Phone_Nr_In_Borrower_Info' at Center.", repo.Salesforce_Website_Chrome.Borrower_Information.Mobile_Phone_Nr_In_Borrower_InfoInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Borrower_Information.Mobile_Phone_Nr_In_Borrower_Info.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Tab}' with focus on 'Salesforce_Website_Chrome.Borrower_Information.Mobile_Phone_Nr_In_Borrower_Info'.", repo.Salesforce_Website_Chrome.Borrower_Information.Mobile_Phone_Nr_In_Borrower_InfoInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Borrower_Information.Mobile_Phone_Nr_In_Borrower_Info.PressKeys("{Tab}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value=$MobileNum) on item 'Salesforce_Website_Chrome.Borrower_Information.Mobile_Phone_Nr_In_Borrower_Info'.", repo.Salesforce_Website_Chrome.Borrower_Information.Mobile_Phone_Nr_In_Borrower_InfoInfo, new RecordItemIndex(2));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Borrower_Information.Mobile_Phone_Nr_In_Borrower_InfoInfo, "Value", MobileNum);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
